<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/landing', function() {
  return view('landing');
})->name('landing');

Route::get('/', 'MagangController@landing')->name('welcome'); // untuk ke welcome
Route::get('/tugas/{variabel}/{data}', 'MagangController@tugas')->name('tugas'); // untuk ke view tugas

// Untuk Route CR
Route::get('/crud/index', 'CrudController@index')->name('crud.index');
Route::get('/crud/create', 'CrudController@create')->name('crud.create');
Route::post('/crud/store', 'CrudController@store')->name('crud.store');

// Untuk Route UD
Route::get('/crud/edit/{crud}', 'CrudController@edit')->name('crud.edit');
Route::put('/crud/update/{crud}', 'CrudController@update')->name('crud.update');
Route::delete('/crud/destroy/{crud}', 'CrudController@destroy')->name('crud.destroy');
