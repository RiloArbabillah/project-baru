<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
  protected $table = 'mahasiswa';

  protected $guarded = [
    'id', 'updated_at', 'created_at',
  ];
}
