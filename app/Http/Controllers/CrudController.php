<?php

namespace App\Http\Controllers;

use Request;
// model / table kita
use App\Mahasiswa;

class CrudController extends Controller
{
    public function index()
    {
      // mengambil data dari table/model mahasiswa
      $mahasiswa = Mahasiswa::all();
      return view('crud.index', compact('mahasiswa'));
    }

    public function create()
    {
      return view('crud.form');
    }

    public function store()
    {
      $nama = Request::get('nama');
      $nim = Request::get('nim');
      $alamat = Request::get('alamat');

      Mahasiswa::create([
        'nama' => $nama,
        'nim' => $nim,
        'alamat' => $alamat,
      ]);

      return redirect()->route('crud.index');
    }

    public function edit($id)
    {
        dd('stop');
    }

    public function update($id)
    {
        dd('stop');
    }

    public function destroy($id)
    {
        dd('stop');
    }
}
