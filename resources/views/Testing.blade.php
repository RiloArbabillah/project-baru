<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Halaman Tes</title>

    {{-- style tailwind --}}
    <link rel="stylesheet" href="/css/app.css">

</head>
<body class="">
    <div class="h-screen w-full text-2xl flex flex-col justify-center items-center">
        <div class="text-blue-500 text-6xl">
            {{$variabel}} {{$data}}
        </div>
        <div class="text-blue-500 text-4xl">
            Lorem ipsum dolor
        </div>
        <div class="text-red-500">
            sit amet consectetur adipisicing elit.
        </div>
        <a href="{{ route('welcome') }}" class="px-5 py-3 bg-red-500 text-white hover:opacity-75 rounded-md">
            Kembali
        </a>
    </div>
</body>
</html>
