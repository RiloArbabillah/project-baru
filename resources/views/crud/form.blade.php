{{-- File yg mau digunakan sebagai parent --}}
@extends('_layouts.default')
{{-- lokasi untuk menaruh posisi file ini pada file parent --}}
@section('content')
  <div class="bg-white p-10 rounded-lg w-2/3">
    <form action="{{route('crud.store')}}" method="POST" class="">
      @csrf
      <div class="space-y-8 mb-10">
        <div class="space-y-2">
          <label for="nama" class="capitalize">nama :</label>
          <input type="text" name="nama" class="border rounded-lg w-full py-2 px-2 focus:outline-none">
        </div>
        <div class="space-y-2">
          <label for="nim" class="capitalize">nim :</label>
          <input type="text" name="nim" class="border rounded-lg w-full py-2 px-2 focus:outline-none">
        </div>
        <div class="space-y-2">
          <label for="alamat" class="capitalize">alamat :</label>
          <input type="text" name="alamat" class="border rounded-lg w-full py-2 px-2 focus:outline-none">
        </div>
      </div>
      <div class="bg-blue-200 -mr-10 -ml-10 -mb-10 px-10 py-5" style="border-radius: 0 0 0.5rem 0.5rem;">
        <button type="submit" class="bg-custom-blue-1 px-4 py-2 rounded-lg hover:opacity-50">
          Simpan
        </button>
      </div>
    </form>
  </div>
@endsection
