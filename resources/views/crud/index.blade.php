@extends('_layouts.default')
@section('content')
  <div class="">
    <div class="w-full flex justify-end">
      <a href="{{route('crud.create')}}" class="px-4 py-2 bg-custom-blue-1 rounded-lg">
        Tambah Mahasiswa
      </a>
    </div>
    <table class="rounded-lg w-full mt-8">
      <thead class="bg-white">
        <tr class="bg-gray-100">
          <th class="border-b py-2 bg-gray-100">No</th>
          <th class="border-b py-2 bg-gray-100">Nama</th>
          <th class="border-b py-2 bg-gray-100">Nim</th>
          <th class="border-b py-2 bg-gray-100">Alamat</th>
        </tr>
      </thead>
      <tbody class="bg-white">
        @foreach ($mahasiswa as $index => $item)
        <tr class="hover:bg-gray-100 border-b">
          <td class="text-center py-2">
            {{ $index + 1 }}
          </td>
          <td class="text-center py-2">
            {{ $item->nama }}
          </td>
          <td class="text-center py-2">
            {{ $item->nim }}
          </td>
          <td class="text-center py-2">
            {{ $item->alamat }}
          </td>
        </tr>
        @endforeach
        @if ($mahasiswa->count() == 0)
          <tr class="hover:bg-gray-100">
            <td colspan="10" class="text-center py-2">Tidak Ada Data</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
@endsection
