<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Tutorial Laravel 7</title>

  {{-- untuk menggunkan tailwind --}}
  <link rel="stylesheet" href="/css/app.css">
</head>
<body class="bg-gray-300 text-gray-700">
  <div class="flex flex-col h-screen">
    {{-- navbar --}}
    <div class="bg-custom-blue-2 flex justify-center items-center h-1/10 space-x-5">
      <a href="{{route('crud.index')}}" class="py-2 px-4 rounded-lg bg-custom-blue-1 hover:opacity-75">Crud</a>
      <a href="#" onclick="active()" class="py-2 px-4 rounded-lg bg-custom-blue-1 hover:opacity-75" id="drop">Drop</a>
        <div id="drop2" class="hidden absolute top-10 right-0">
          <a href="" class="py-2 px-4 rounded-lg bg-custom-blue-1 hover:opacity-75">dropdown 1</a>
          <a href="" class="py-2 px-4 rounded-lg bg-custom-blue-1 hover:opacity-75">dropdown 2</a>
          <a href="" class="py-2 px-4 rounded-lg bg-custom-blue-1 hover:opacity-75">dropdown 3</a>
        </div>

      <a href="" onclick="hide()" class="py-2 px-4 rounded-lg bg-custom-blue-1 hover:opacity-75">None</a>
    </div>


    {{-- tempat untuk lokasi child/anak --}}
    <div class="flex flex-grow  overflow-hidden h-9/10 w-11/12">

      <div class="bg-custom-blue-2 flex flex-col justify-center items-center h-screen space-y-5">
        <a href="{{route('crud.index')}}" class="py-2 px-4 rounded-lg bg-custom-blue-1 hover:opacity-75">Crud</a>
        <a href="#" onclick="active()" class="py-2 px-4 rounded-lg bg-custom-blue-1 hover:opacity-75" id="drop">Drop</a>
        <a href="" onclick="hide()" class="py-2 px-4 rounded-lg bg-custom-blue-1 hover:opacity-75">None</a>
      </div>
        <div class="overflow-y-auto px-20 py-5 w-full h-full">
          @yield('content')
        </div>
      </div>
  </div>
</body>
<script>
  var drop = false;
  function active(){
    drop = true;
    var drop2 = document.getElementById('drop2');
    drop2.classList.remove('hidden')
  }
  function hide(){
    var drop2 = document.getElementById('drop2');
    drop2.classList.add('hidden')
  }
</script>
</html>
